<!DOCTYPE html>

<html lang="es">

<head>
	<meta charset="utf-8">
	<title>Ejercicio 1</title>
	<link rel="stylesheet" href="../style_guide.css">

</head>

<body>
	
	<h2>TABLA NÚMEROS DEL 1 AL 100</h2>
	<table id = "ej1_table"; border = "1";>
       
    <?php
    // contador para imprimir los números
    $cont=1;
    
    // ciclo para ir mostrando filas
    for($x= 0; $x<=9; $x++){
	   // imprime filas	
	   echo "<tr>";
	   // ciclo para ir mostrando columnas
	   for($y=0; $y<=9; $y++){
		   // imprime columnas
		   echo "<td>". $cont. "</td>";
		   $cont++;
		   }
		echo "</tr>";
	}
       ?>
    </table>
</body>

</html>
