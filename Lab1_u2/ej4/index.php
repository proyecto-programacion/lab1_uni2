<!DOCTYPE html>

<html lang="es">

<head>
	<meta charset="utf-8">
	<title>Ejercicio 4</title>
	<link rel="stylesheet" href="../style_guide.css">

</head>

<body>
	<!-- titulo de la página -->
	<h2>TABLA 4X4 CON IMÁGENES</h2>
    
    <!-- Primera parte código php-->
    <?php
	
	//Variable para guardar el directorio fotos
    $dir = "fotos/";
    
    // Abre el directorio y su contenido
    if(is_dir($dir))
    {	
		// Condiciones al abrir el directorio
		if ($dh = opendir($dir))
		{	
			// Se crea la tabla
			echo "<table border=3; width =auto>";
			echo "<tr>";
			// contador para que se separen las fotos en cada fila
			$cont=0;
			
			// Se lee la info contenido en el directorio
			while(($archivo = readdir($dh)) !== false)
			{
				if($archivo != "." && $archivo != "..")
				{
					if($cont == 4)
					{
						$cont=0;
						echo "</tr>";
						echo "<tr>";
					}
					$cont++;
	?>				
					<!-- Muestra las imágenes contenidas-->
					<td><img src=<?php echo $dir . $archivo ?> width= "300px"</td>
	<?php
				}
			}
			echo "</tr>";
			echo "</table>";
			//Se cierra el directorio
			closedir($dh);
		}
	}
       ?>
    </table>
</body>

</html>
