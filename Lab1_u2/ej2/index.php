<!DOCTYPE html>

<html lang="es">

<head>
	<meta charset="utf-8">
	<title>Ejercicio 2</title>
	<link rel="stylesheet" href="../style_guide.css">

</head>

<body>
	
	<h2>Tabla de 15 filas y 15 columnas con números del 1 al 15x15</h2>
	<table border="1"; width ="100%">
	
	<?php
	
	$cont=1;
    $tam=15;
     
     // ciclo para ir mostrando filas
    for($x= 0; $x<=$tam-1; $x++){
	   // imprime filas
	   if(($x %2) == 0){
		   echo "<tr bgcolor = darkgrey>";
		   }
		   else{
			   echo "<tr bgcolor = white>";
			   }
	   // ciclo para ir mostrando columnas
	   for($y=0; $y<=$tam-1; $y++){
		   // imprime columnas
		   echo "<td>". $cont. "</td>";
		   $cont++;
		   }
		echo "</tr>";
		}
    ?>
    </table>
</body>

</html>
